﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followPlayer : MonoBehaviour
{
    public GameObject player;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = convertVector(player.transform.position) + offset;
    }

    Vector3 convertVector(Vector2 v)
    {

        return new Vector3(v.x, v.y, 0.0f);
    }
}
