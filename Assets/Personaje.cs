﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    public Transform objetivo;
    public Nodo[] path;
    public float rango;

    private int actual;

    public Animator anim;
    private float velocidad;
    private bool morido;
    // Start is called before the first frame update
    void Start()
    {
        actual = 0;
        StartCoroutine(ChecarSiLlegue());
        anim= GetComponent<Animator>();
        velocidad = 200f;
        morido = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (morido == false)
        {
            transform.LookAt(path[actual].transform);
            transform.Translate(transform.forward * Time.deltaTime * 5, Space.World);
            anim.SetFloat("velX", velocidad * Time.deltaTime);
            anim.SetFloat("velY", velocidad * Time.deltaTime);
        }
        

        
        if(Input.GetMouseButtonUp(0))
        {
            Ray disparo = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(disparo, out hit))
            {
                print("Si pego " + hit.point);
                print("EL enemigo " + hit.transform.name);
                //velocidad = 0;

                if (hit.transform.tag == "Enemigo")
                {
                    morido = true;
                    StartCoroutine(matar());
                }

            }

        }
    }
    IEnumerator matar()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezePosition;
        // triggered
        anim.SetTrigger("muerto");

        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
        StopCoroutine(matar());
    }

    IEnumerator ChecarSiLlegue()
    {
        float d;
        while (true)
        {
            yield return new WaitForSeconds(1f);
            d = Vector3.Distance(transform.position, path[actual].transform.position);
            if (d < rango)
            {
                actual++;
                actual %= path.Length;
            }
        }
    }
}
