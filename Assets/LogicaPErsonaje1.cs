﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicaPErsonaje1 : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotacion = 200f;
    public Animator anim;
    public float x, y;

    private GameObject enemigo;
    public Proyectil projectil;
    public Transform referencia;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(projectiles());
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.z, 10);
        Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
        lookPos = lookPos - transform.position;
        float angle = Mathf.Atan2(lookPos.z, lookPos.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.down); // Turns Right
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        transform.Rotate(0, x * Time.deltaTime * velocidadRotacion, 0);
        transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);

        anim.SetFloat("velX", x);
        anim.SetFloat("velY", y);

        
    }
    IEnumerator projectiles()
    {
        yield return new WaitForSeconds(1f);
        Instantiate(projectil, 
            new Vector3(Mathf.Clamp(referencia.position.x,-2f,2f), Mathf.Clamp(transform.position.y,-4f,4f),transform.position.z)
            ,transform.rotation);
    }
}
